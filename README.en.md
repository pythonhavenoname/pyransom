# PyRansom

#### Description
PyRansom - A python3-based free open source ransomware
一个基于python3的免费开源勒索软件

#### Usage
For stability, this ransomware does not use any third-party libraries! (there is only one master file) you can use
pip install pyinstaller
and then
cd path
pyinstaller -w -F PyRansom.py

#### Png
![Ransom](%E6%95%88%E6%9E%9C.png223216hds93r98wd0zcsw3.png)