# PyRansom

#### 介绍
PyRansom - A python3-based free open source ransomware
一个基于python3的免费开源勒索软件
#### 用法
为了稳定型，这款勒索软件没有使用任何第三方库！(只有一个主文件)
您可以使用pip install pyinstaller
cd Path
然后pyinstaller -w -F Pyransom.py
等一会，然后在工作目录中的dist目录中就会有一个exe文件了
当然，你也可以在使用pyinstaller时加入其他参数。
#### 效果
![Ransom](%E6%95%88%E6%9E%9C.png223216hds93r98wd0zcsw3.png)
![new](acepy.jpgacepy.jpg)
![icon](icon.icoicon.gif)
![360](360.png%E5%AE%89%E5%85%A8%E5%8D%AB%E5%A3%AB%E5%8B%92%E7%B4%A2%E7%97%85%E6%AF%92%E4%B8%93%E9%A2%98_%E6%96%87%E4%BB%B6%E6%81%A2%E5%A4%8D_%E5%AE%89%E5%85%A8%E5%8D%AB%E5%A3%AB%E7%A6%BB%E7%BA%BF%E6%95%91%E7%81%BE%E7%89%88_%E6%96%87%E6%A1%A3%E5%8D%AB%E5%A3%AB.png)
![火绒](https://fj.kafan.cn/attachment/forum/202301/24/175753y8srkftrffj5tojf.jpg)
#### 仅供研究，勿用非法！
各大厂商，皆已上报，若用非法，作者概不承担！